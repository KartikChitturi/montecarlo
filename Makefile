make: rand_uniform rand_exp rand_normal pi integral_unbiased integral_importance

rand_uniform: rand_uniform.c
	icc rand_uniform.c -o rand_uniform
	
rand_exp: rand_exp.c
	icc rand_exp.c -o rand_exp
	
rand_normal: rand_normal.c
	icc rand_normal.c -o rand_normal
	
pi: pi.c
	icc pi.c -o pi

integral_unbiased: integral_unbiased.c
	icc integral_unbiased.c -o integral_unbiased

integral_importance: integral_importance.c
	icc integral_importance.c -o integral_importance
